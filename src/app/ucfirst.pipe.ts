import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ucfirst'
})
export class UcfirstPipe implements PipeTransform {

  transform(value: string): any {
    // the quick brown fox jumped over the lazy dog
    // [the, quick, brown]
    return value.split(' ')
      .map((word: string) => ' ' + word.substring(0, 1)
        .toLocaleUpperCase() + word.substring(1))
      .join('');
  }

}
